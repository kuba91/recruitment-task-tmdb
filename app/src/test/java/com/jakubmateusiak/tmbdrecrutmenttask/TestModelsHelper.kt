package com.jakubmateusiak.tmbdrecrutmenttask

import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie

object TestModelsHelper {

    fun getMovieModel(favourite: Boolean): Movie {
        return Movie(
            1234,
            "image",
            "title",
            "releadeDate",
            "overview",
            favourite
        )
    }
}