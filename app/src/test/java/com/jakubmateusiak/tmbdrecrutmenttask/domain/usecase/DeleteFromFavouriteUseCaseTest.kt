package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.jakubmateusiak.tmbdrecrutmenttask.BaseRobolectricTest
import com.jakubmateusiak.tmbdrecrutmenttask.TestModelsHelper
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.datasource.RemoteDataSource
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.entities.MovieEntity
import com.jakubmateusiak.tmbdrecrutmenttask.data.repository.MoviesRepositoryImpl
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class DeleteFromFavouriteUseCaseTest : BaseRobolectricTest() {

    @Test
    fun `deleting from favourite test should return 1`() = runBlocking {
        val remoteDataSource = mock<RemoteDataSource> { }

        val movie = TestModelsHelper.getMovieModel(false)
        val moviesRepository = MoviesRepositoryImpl(remoteDataSource, db.moviesDao())

        db.moviesDao().insertFavourite(
            MovieEntity(
                movie.id,
                movie.image, movie.title, movie.releaseDate, movie.overview, movie.favourite
            )
        )

        val useCase = DeleteFromFavouriteUseCase(moviesRepository)

        val result = useCase.execute(movie)

        assertEquals(1, result)
    }

    @Test
    fun `deleting from favourite where movie is absent test should return 0`() = runBlocking {
        val remoteDataSource = mock<RemoteDataSource> { }

        val movie = TestModelsHelper.getMovieModel(false)
        val moviesRepository = MoviesRepositoryImpl(remoteDataSource, db.moviesDao())

        val useCase = DeleteFromFavouriteUseCase(moviesRepository)

        val result = useCase.execute(movie)

        assertEquals(0, result)
    }

}