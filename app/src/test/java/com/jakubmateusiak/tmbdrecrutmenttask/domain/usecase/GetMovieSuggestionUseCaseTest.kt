package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import com.jakubmateusiak.tmbdrecrutmenttask.BaseRobolectricTest
import com.jakubmateusiak.tmbdrecrutmenttask.TestModelsHelper
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.datasource.RemoteDataSource
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.ApiResult
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.MovieSearchResponse
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.Result
import com.jakubmateusiak.tmbdrecrutmenttask.data.repository.MoviesRepositoryImpl
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.ErrorableObjectContainer
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class GetMovieSuggestionUseCaseTest : BaseRobolectricTest(){

    @Test
    fun `when search should return empty list test`() = runBlocking {
        val remoteDataSource = mock<RemoteDataSource> {
            val response = MovieSearchResponse(1, emptyList(),1,0)

            onBlocking { search(anyString()) } doReturn ApiResult.Success<MovieSearchResponse>(response)
        }
        val moviesRepository = MoviesRepositoryImpl(remoteDataSource, db.moviesDao())
        val useCase = GetMovieSuggestionUseCase(moviesRepository)

        val result = useCase.execute("test")

        assertTrue(result is ErrorableObjectContainer.Success)
        assertEquals(emptyList<Movie>(), (result as ErrorableObjectContainer.Success).item)
    }

    @Test
    fun `when search should return list with 2 items test`() = runBlocking {
        val remoteDataSource = mock<RemoteDataSource> {
            val response = MovieSearchResponse(1, listOf(
                Result(false,"", emptyList(), 1,"pl","title","overview",10.0,null,null,"title",false, 10.0,2),
                Result(false,"", emptyList(), 2,"pl","title","overview",10.0,null,null,"title",false, 10.0,2)
            ),1,0)

            onBlocking { search(anyString()) } doReturn ApiResult.Success<MovieSearchResponse>(response)
        }
        val moviesRepository = MoviesRepositoryImpl(remoteDataSource, db.moviesDao())
        val useCase = GetMovieSuggestionUseCase(moviesRepository)

        val result = useCase.execute("test")

        assertTrue(result is ErrorableObjectContainer.Success)
        assertEquals(2, (result as ErrorableObjectContainer.Success).item.size)
    }

    @Test
    fun `when search should return error test`() = runBlocking {
        val remoteDataSource = mock<RemoteDataSource> {
            val response = MovieSearchResponse(1, emptyList(),1,0)

            onBlocking { search(anyString()) } doReturn ApiResult.Error("error")
        }
        val moviesRepository = MoviesRepositoryImpl(remoteDataSource, db.moviesDao())
        val useCase = GetMovieSuggestionUseCase(moviesRepository)

        val result = useCase.execute("test")

        assertTrue(result is ErrorableObjectContainer.Error)
        assertEquals("error", (result as ErrorableObjectContainer.Error).error)
    }

}