package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import com.jakubmateusiak.tmbdrecrutmenttask.BaseRobolectricTest
import com.jakubmateusiak.tmbdrecrutmenttask.TestModelsHelper
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.datasource.RemoteDataSource
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.entities.MovieEntity
import com.jakubmateusiak.tmbdrecrutmenttask.data.repository.MoviesRepositoryImpl
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class SaveFavouriteUseCaseTest : BaseRobolectricTest(){

    @Test
    fun `adding to favourite test`() = runBlocking {
        val remoteDataSource = mock<RemoteDataSource> { }

        val movie = TestModelsHelper.getMovieModel(false)
        val moviesRepository = MoviesRepositoryImpl(remoteDataSource, db.moviesDao())

        db.moviesDao().insertFavourite(
            MovieEntity(
                movie.id,
                movie.image, movie.title, movie.releaseDate, movie.overview, movie.favourite
            )
        )

        val useCase = SaveFavouriteUseCase(moviesRepository)

        val result = useCase.execute(movie)

        assertEquals(1234, result)
    }

}