package com.jakubmateusiak.tmbdrecrutmenttask

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.AppDatabase
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared.DispatchersContainer
import kotlinx.coroutines.Dispatchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.koin.core.context.stopKoin
import org.robolectric.RuntimeEnvironment

open class BaseRobolectricTest {

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    lateinit var db: AppDatabase
    lateinit var dispatchers: DispatchersContainer

    @Before
    open fun setup() {
        dispatchers = DispatchersContainer(Dispatchers.Main, Dispatchers.Main)
        db = Room.inMemoryDatabaseBuilder(RuntimeEnvironment.application, AppDatabase::class.java)
            .allowMainThreadQueries().build()
    }

    @After
    open fun after() {
        db.close()
        stopKoin()
    }
}
