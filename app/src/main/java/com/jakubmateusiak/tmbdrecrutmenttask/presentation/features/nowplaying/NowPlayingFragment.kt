package com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.nowplaying

import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakubmateusiak.tmbdrecrutmenttask.R
import com.jakubmateusiak.tmbdrecrutmenttask.databinding.FragmentNowPlayingBinding
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared.BaseAdapter
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared.BaseFragmentWithBindings
import kotlinx.android.synthetic.main.fragment_now_playing.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class NowPlayingFragment : BaseFragmentWithBindings<FragmentNowPlayingBinding>(),
    NowPlayingRVAdapter.NowPlayingRVAdapterListener, BaseAdapter.OnItemClickListener<Movie> {

    private val viewModel: NowPlayingViewModel by viewModel()
    private val adapter = NowPlayingRVAdapter()
    private lateinit var searchAdapter: SearchAdapter

    override fun getLayoutId(): Int = R.layout.fragment_now_playing

    override fun bindObservers(binding: FragmentNowPlayingBinding) {
        super.bindObservers(binding)
        viewModel.moviesMediatorLiveData.observe(viewLifecycleOwner, Observer {
            adapter.items = it
        })
        viewModel.moviesSuggestions.observe(viewLifecycleOwner, Observer{
            searchAdapter.clear()
            searchAdapter.addAll(it.map {
                it.title
            })
            searchAdapter.filter.filter(svMovie.text.toString())
        })

        viewModel.viewState.observe(viewLifecycleOwner, Observer{
            when(it){
                is NowPlayingFragmentState.Loading -> {
                    clLoadingContent.visibility = View.VISIBLE
                    clLoadedContent.visibility = View.GONE
                    clError.visibility = View.GONE
                }
                is NowPlayingFragmentState.Loaded -> {
                    clLoadingContent.visibility = View.GONE
                    clLoadedContent.visibility = View.VISIBLE
                    clError.visibility = View.GONE
                }
                is NowPlayingFragmentState.Error -> {
                    clLoadingContent.visibility = View.GONE
                    clLoadedContent.visibility = View.GONE
                    clError.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun prepareView(savedInstanceState: Bundle?) {
        rvNowPlayingMovies.layoutManager = LinearLayoutManager(requireContext())
        rvNowPlayingMovies.adapter = adapter
        adapter.favouriteClickListener = this
        adapter.listener = this
        searchAdapter = SearchAdapter(requireContext())
        svMovie.setAdapter(searchAdapter)
        svMovie.doOnTextChanged { text, _, _, _ ->
            viewModel.movieQueryChanged(text.toString())
        }
    }

    override fun onFavouriteClicked(movie: Movie) {
        viewModel.onFavouriteClicked(movie)
    }

    override fun onItemClick(item: Movie) {
        val directions = NowPlayingFragmentDirections.actionNowPlayingFragmentToMovieFragment(item)
        NavHostFragment.findNavController(this).navigate(directions)
    }

    fun retryBtnClicked(){
        viewModel.refreshMovies()
    }
}
