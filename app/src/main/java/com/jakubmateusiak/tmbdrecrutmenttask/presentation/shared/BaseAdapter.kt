package com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    interface OnItemClickListener<T> {
        fun onItemClick(item: T)
    }

    var listener: OnItemClickListener<T>? = null

    var items: List<T> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return items.size
    }
}
