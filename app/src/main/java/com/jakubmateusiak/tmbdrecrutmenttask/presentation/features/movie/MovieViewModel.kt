package com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.movie

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase.DeleteFromFavouriteUseCase
import com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase.SaveFavouriteUseCase
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared.DispatchersContainer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MovieViewModel(
    val movie: Movie,
    private val saveFavouriteUseCase: SaveFavouriteUseCase,
    private val deleteFromFavouriteUseCase: DeleteFromFavouriteUseCase,
    private val dispatchersContainer: DispatchersContainer
) : ViewModel() {

    val movieObservable = ObservableField<Movie>()

    init {
        movieObservable.set(movie)
    }

    fun switchFavouriteStatus(){
        CoroutineScope(dispatchersContainer.main).launch {
            movieObservable.get()?.let {
                if(it.favourite){
                    withContext(dispatchersContainer.io){deleteFromFavouriteUseCase.execute(movie)}
                    print("blabla 3 ${movieObservable.get()}")
                    it.favourite = false
                }else{
                    withContext(dispatchersContainer.io){saveFavouriteUseCase.execute(movie)}
                    print("blabla 4 ${movieObservable.get()}")
                    it.favourite = true
                }
                movieObservable.notifyChange()
            }

        }

    }

}