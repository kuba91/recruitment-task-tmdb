package com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.nowplaying

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jakubmateusiak.tmbdrecrutmenttask.R
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.RESTClient
import com.jakubmateusiak.tmbdrecrutmenttask.databinding.ItemNowPlayingBinding
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared.BaseAdapter

class NowPlayingRVAdapter : BaseAdapter<Movie, NowPlayingRVAdapter.NowPlayingViewHolder>() {

    var favouriteClickListener: NowPlayingRVAdapterListener? = null

    interface NowPlayingRVAdapterListener{
        fun onFavouriteClicked(movie: Movie)
    }

    inner class NowPlayingViewHolder(val binding: ItemNowPlayingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: Movie) {
            binding.movie = movie
            binding.handler = this
            binding.executePendingBindings()
            itemView.setOnClickListener { listener?.onItemClick(items[adapterPosition]) }
        }

        fun onFavouriteClicked(){
            favouriteClickListener?.onFavouriteClicked(items[adapterPosition])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NowPlayingViewHolder {
        return NowPlayingViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_now_playing,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NowPlayingViewHolder, position: Int) {
        holder.bind(items[position])
    }
}