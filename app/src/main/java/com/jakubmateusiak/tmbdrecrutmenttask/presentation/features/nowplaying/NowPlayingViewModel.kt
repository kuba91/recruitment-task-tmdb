package com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.nowplaying

import android.util.Log
import androidx.lifecycle.*
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.ErrorableObjectContainer
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase.*
import kotlinx.coroutines.*

class NowPlayingViewModel(
    private val getNowPlayingUseCase: GetNowPlayingUseCase,
    private val deleteFromFavouriteUseCase: DeleteFromFavouriteUseCase,
    private val saveFavouriteUseCase: SaveFavouriteUseCase,
    private val getFavouritesMoviesUseCase: GetFavouritesMoviesUseCase,
    private val getMovieSuggestionUseCase: GetMovieSuggestionUseCase
) : ViewModel() {

    var favouritesMoviesIdsLiveData: LiveData<List<Int>>? = null
    val moviesMediatorLiveData = MediatorLiveData<List<Movie>>()
    var moviesSuggestions = MutableLiveData<List<Movie>>()
    var searchJob: Job? = null
    val viewState = MutableLiveData<NowPlayingFragmentState>(NowPlayingFragmentState.Loading)

    init {
        getMovies()
    }

    private fun getMovies() {
        CoroutineScope(Dispatchers.Main).launch {
            favouritesMoviesIdsLiveData =
                withContext(Dispatchers.IO) { getFavouritesMoviesUseCase.execute() }
            favouritesMoviesIdsLiveData?.let {
                moviesMediatorLiveData.addSource(it, Observer { favouritesIds ->
                    val newItems = moviesMediatorLiveData.value
                    newItems?.let { movies ->
                        combineFavouritesWithMovies(movies, favouritesIds)
                        moviesMediatorLiveData.postValue(newItems)
                    }
                })
            }

            when (val response = withContext(Dispatchers.IO) { getNowPlayingUseCase.execute() }) {
                is ErrorableObjectContainer.Success -> {
                    favouritesMoviesIdsLiveData?.value?.let {
                        viewState.postValue(NowPlayingFragmentState.Loaded)
                        combineFavouritesWithMovies(response.item, it)
                    }
                    moviesMediatorLiveData.value = response.item
                }
                is ErrorableObjectContainer.Error -> {
                    viewState.postValue(NowPlayingFragmentState.Error(response.error))
                }
            }
        }
    }

    private fun combineFavouritesWithMovies(movies: List<Movie>, favouritesIds: List<Int>) {
        movies.map { movie ->
            movie.favourite = favouritesIds.contains(movie.id)

        }
    }

    fun onFavouriteClicked(movie: Movie) {
        CoroutineScope(Dispatchers.Main).launch {
            if (movie.favourite) {
                withContext(Dispatchers.IO) {
                    deleteFromFavouriteUseCase.execute(
                        movie
                    )
                }
            } else {
                withContext(Dispatchers.IO) {
                    saveFavouriteUseCase.execute(
                        movie
                    )
                }
            }
        }
    }

    fun movieQueryChanged(query: String) {
        searchJob?.cancel()
        searchJob = CoroutineScope(Dispatchers.Main).launch {
            delay(500)
            val response = withContext(Dispatchers.IO) { getMovieSuggestionUseCase.execute(query) }
            if (response is ErrorableObjectContainer.Success) {
                Log.d("myApp","${response.item}")
                moviesSuggestions.postValue(response.item)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }

    fun refreshMovies() {
        getMovies()
    }
}
