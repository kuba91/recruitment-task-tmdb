package com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

data class DispatchersContainer(val main : CoroutineDispatcher = Dispatchers.Main, val io: CoroutineDispatcher = Dispatchers.IO)