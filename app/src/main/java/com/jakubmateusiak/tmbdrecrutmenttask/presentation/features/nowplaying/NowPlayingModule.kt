package com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.nowplaying

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val nowPlayingModule = module {
    viewModel { NowPlayingViewModel(get(), get(), get(), get(), get()) }
}
