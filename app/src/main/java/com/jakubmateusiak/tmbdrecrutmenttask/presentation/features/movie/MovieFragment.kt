package com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.movie

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import com.jakubmateusiak.tmbdrecrutmenttask.R
import com.jakubmateusiak.tmbdrecrutmenttask.databinding.FragmentMovieBinding
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared.BaseFragmentWithBindings
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MovieFragment : BaseFragmentWithBindings<FragmentMovieBinding>() {

    private val viewModel: MovieViewModel by viewModel { parametersOf(getMovieFromArgs()) }
    private var favouriteMenuItem: MenuItem? = null

    private val movieObserver = object : Observable.OnPropertyChangedCallback(){
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            val favourite = (sender as ObservableField<Movie>).get()?.favourite
            favourite?.let { setFavouriteIcon(it) }

        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_movie

    override fun bindObservers(binding: FragmentMovieBinding) {
        super.bindObservers(binding)
        binding.movie = viewModel.movieObservable
        viewModel.movieObservable.addOnPropertyChangedCallback(movieObserver)
    }

    override fun prepareView(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        requireActivity().title = getMovieFromArgs().title
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.movieObservable.removeOnPropertyChangedCallback(movieObserver)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_movie, menu)
        favouriteMenuItem = menu.findItem(R.id.actionFavourite)
        setFavouriteIcon(getMovieFromArgs().favourite)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setFavouriteIcon(favourite: Boolean) {
        favouriteMenuItem?.icon =
            resources.getDrawable(
                if (favourite) R.drawable.ic_baseline_star_24 else R.drawable.ic_baseline_star_border_24
            )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionFavourite -> {
                viewModel.switchFavouriteStatus()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun getMovieFromArgs(): Movie {
        return MovieFragmentArgs.fromBundle(requireArguments()).movie

    }

}

