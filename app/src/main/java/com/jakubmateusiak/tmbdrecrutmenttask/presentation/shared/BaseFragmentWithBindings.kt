package com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragmentWithBindings<T : ViewDataBinding> : Fragment() {

    override fun
            onCreateView(
                inflater: LayoutInflater,
                container: ViewGroup?,
                savedInstanceState: Bundle?
            ): View {
        val binding: T = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        bindObservers(binding)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareView(savedInstanceState)
    }

    open fun bindObservers(binding: T) {
        binding.lifecycleOwner = viewLifecycleOwner
    }

    abstract fun prepareView(savedInstanceState: Bundle?)

    @LayoutRes abstract fun getLayoutId(): Int
}
