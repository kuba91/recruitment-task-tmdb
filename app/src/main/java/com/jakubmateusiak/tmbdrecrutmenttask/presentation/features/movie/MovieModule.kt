package com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.movie

import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val movieModule = module{
    viewModel { (movie: Movie) -> MovieViewModel(movie, get(), get(), get()) }
}