package com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared

import org.koin.dsl.module

val utilsModule  = module{
    single { DispatchersContainer() }
}