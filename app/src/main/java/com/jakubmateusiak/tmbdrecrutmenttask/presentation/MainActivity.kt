package com.jakubmateusiak.tmbdrecrutmenttask.presentation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.jakubmateusiak.tmbdrecrutmenttask.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    NavController.OnDestinationChangedListener {
    private val topLevelDestinations: AppBarConfiguration = AppBarConfiguration
        .Builder(
            R.id.nowPlayingFragment
        )
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        getNavController().addOnDestinationChangedListener(this)

        toolbar.setNavigationOnClickListener {
            onSupportNavigateUp()
        }

    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        val isTopFragment =
            destination.id == R.id.nowPlayingFragment
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(!isTopFragment)
            setDisplayHomeAsUpEnabled(!isTopFragment)
        }
        if (destination.label.toString() != "null") {
            toolbar.title = destination.label.toString()
        }
    }

    private fun getNavController(): NavController {
        return findNavController(R.id.nav_host_fragment)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(getNavController(), topLevelDestinations)
    }
}
