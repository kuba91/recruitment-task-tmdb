package com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.nowplaying

sealed class NowPlayingFragmentState {
    object Loading : NowPlayingFragmentState()
    object Loaded : NowPlayingFragmentState()
    class Error(val error: String) : NowPlayingFragmentState()

}