package com.jakubmateusiak.tmbdrecrutmenttask.presentation.bindings

import android.media.Image
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.jakubmateusiak.tmbdrecrutmenttask.R
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.RESTClient

@BindingAdapter("imdbImage")
fun setImageResource(imageView: ImageView, image: String) {
    Glide.with(imageView.context).load(
        RESTClient.IMAGE_PATH+image
    ).into(imageView)
}