package com.jakubmateusiak.tmbdrecrutmenttask

import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.apiModule
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.databaseModule
import com.jakubmateusiak.tmbdrecrutmenttask.domain.repository.repositoriesModule
import com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase.useCasesModule
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.movie.movieModule
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.features.nowplaying.nowPlayingModule
import com.jakubmateusiak.tmbdrecrutmenttask.presentation.shared.utilsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    apiModule,
                    databaseModule,
                    repositoriesModule,
                    useCasesModule,
                    utilsModule,
                    nowPlayingModule,
                    movieModule
                )
            )
        }
        Stetho.initializeWithDefaults(this@App)

    }
}
