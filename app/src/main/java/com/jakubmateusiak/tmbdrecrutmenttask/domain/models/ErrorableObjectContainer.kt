package com.jakubmateusiak.tmbdrecrutmenttask.domain.models

sealed class ErrorableObjectContainer<out T : Any> {
    class Success<out T : Any>(val item: T) : ErrorableObjectContainer<T>()
    class Error(val error: String) : ErrorableObjectContainer<Nothing>()
}
