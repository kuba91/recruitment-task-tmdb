package com.jakubmateusiak.tmbdrecrutmenttask.domain.repository

import com.jakubmateusiak.tmbdrecrutmenttask.data.repository.MoviesRepositoryImpl
import org.koin.dsl.module

val repositoriesModule = module {
    single<MoviesRepository> { MoviesRepositoryImpl(get(), get()) }
}
