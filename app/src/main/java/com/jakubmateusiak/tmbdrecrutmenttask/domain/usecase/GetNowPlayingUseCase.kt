package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.ErrorableObjectContainer
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.domain.repository.MoviesRepository

class GetNowPlayingUseCase(private val moviesRepository: MoviesRepository) {

    suspend fun execute(): ErrorableObjectContainer<List<Movie>> {
        return moviesRepository.getNowPlayingMovies()
    }
}
