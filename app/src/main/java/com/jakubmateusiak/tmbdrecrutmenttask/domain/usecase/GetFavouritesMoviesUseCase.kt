package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import androidx.lifecycle.LiveData
import com.jakubmateusiak.tmbdrecrutmenttask.domain.repository.MoviesRepository

class GetFavouritesMoviesUseCase(val moviesRepository: MoviesRepository) {

    suspend fun execute(): LiveData<List<Int>> {
        return moviesRepository.getFavouritesIds()
    }

}