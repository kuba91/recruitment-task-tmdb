package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.ErrorableObjectContainer
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.domain.repository.MoviesRepository

class GetMovieSuggestionUseCase(val repository: MoviesRepository) {

    suspend fun execute(query: String): ErrorableObjectContainer<List<Movie>> {
        return repository.search(query)
    }

}