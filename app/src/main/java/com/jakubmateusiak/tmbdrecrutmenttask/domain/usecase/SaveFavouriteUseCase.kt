package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import android.util.Log
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.domain.repository.MoviesRepository

class SaveFavouriteUseCase(private val moviesRepository: MoviesRepository) {

    suspend fun execute(movie: Movie): Long {
        return moviesRepository.saveAsFavourite(movie)
    }

}