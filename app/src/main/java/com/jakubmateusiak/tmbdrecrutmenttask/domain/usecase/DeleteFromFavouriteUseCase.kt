package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.domain.repository.MoviesRepository

class DeleteFromFavouriteUseCase(private val moviesRepository: MoviesRepository) {

    suspend fun execute(movie: Movie): Int {
        return moviesRepository.deleteFavourite(movie.id)
    }

}