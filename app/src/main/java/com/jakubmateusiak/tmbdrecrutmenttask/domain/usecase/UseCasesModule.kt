package com.jakubmateusiak.tmbdrecrutmenttask.domain.usecase

import org.koin.dsl.module

val useCasesModule = module {
    factory { GetNowPlayingUseCase(get()) }
    factory { SaveFavouriteUseCase(get()) }
    factory { DeleteFromFavouriteUseCase(get()) }
    factory { GetFavouritesMoviesUseCase(get()) }
    factory { GetMovieSuggestionUseCase(get()) }
}
