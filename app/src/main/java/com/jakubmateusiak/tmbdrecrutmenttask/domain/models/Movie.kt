package com.jakubmateusiak.tmbdrecrutmenttask.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    val id: Int,
    val image: String,
    val title: String,
    val releaseDate: String,
    val overview: String,
    val vote: Int,
    val voteAverage: Double,
    var favourite: Boolean
) :
    Parcelable
