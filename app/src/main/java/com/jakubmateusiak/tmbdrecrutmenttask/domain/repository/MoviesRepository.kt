package com.jakubmateusiak.tmbdrecrutmenttask.domain.repository

import androidx.lifecycle.LiveData
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.ErrorableObjectContainer
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie

interface MoviesRepository {
    suspend fun search(query: String): ErrorableObjectContainer<List<Movie>>

    suspend fun getNowPlayingMovies(): ErrorableObjectContainer<List<Movie>>

    suspend fun getFavouritesIds() : LiveData<List<Int>>

    suspend fun saveAsFavourite(movie: Movie) : Long

    suspend fun deleteFavourite(movieId: Int) : Int
}
