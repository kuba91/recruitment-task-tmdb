package com.jakubmateusiak.tmbdrecrutmenttask.data.repository

import androidx.lifecycle.LiveData
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.datasource.RemoteDataSource
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.ApiResult
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.MovieSearchResponse
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.NowPlayingResponse
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.dao.MoviesDao
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.entities.MovieEntity
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.ErrorableObjectContainer
import com.jakubmateusiak.tmbdrecrutmenttask.domain.models.Movie
import com.jakubmateusiak.tmbdrecrutmenttask.domain.repository.MoviesRepository

class MoviesRepositoryImpl(private val remoteDataSource: RemoteDataSource, private val moviesDao: MoviesDao) : MoviesRepository {

    override suspend fun search(query: String): ErrorableObjectContainer<List<Movie>>{
        return when (val response = remoteDataSource.search(query)) {
            is ApiResult.Success -> {
                ErrorableObjectContainer.Success(convertToMoviesList(response.data))
            }
            is ApiResult.Error -> ErrorableObjectContainer.Error(response.error)
        }
    }

    override suspend fun getNowPlayingMovies(): ErrorableObjectContainer<List<Movie>> {
        return when (val response = remoteDataSource.getNowPlaying()) {
            is ApiResult.Success -> {
                ErrorableObjectContainer.Success(convertToMoviesList(response.data))
            }
            is ApiResult.Error -> ErrorableObjectContainer.Error(response.error)
        }
    }

    override suspend fun getFavouritesIds(): LiveData<List<Int>> {
        return moviesDao.selectFavourites()
    }

    private fun convertToMoviesList(response: NowPlayingResponse): List<Movie> {
        val movies = ArrayList<Movie>()
        response.results.forEach {
            movies.add(Movie(
                id = it.id,
                image = if(it.poster_path.isNullOrEmpty()) "" else it.poster_path ,
                title = it.title,
                releaseDate = if(it.release_date.isNullOrEmpty()) "" else it.release_date ,
                overview = it.overview,
                vote = it.vote_count,
                voteAverage = it.vote_average,
                favourite = false
            ))
        }
        return movies
    }

    private fun convertToMoviesList(response: MovieSearchResponse): List<Movie> {
        val movies = ArrayList<Movie>()
        response.results.forEach {
            movies.add(Movie(
                id = it.id,
                image = if(it.poster_path.isNullOrEmpty()) "" else it.poster_path ,
                title = it.title,
                releaseDate = if(it.release_date.isNullOrEmpty()) "" else it.release_date ,
                overview = it.overview,
                vote = it.vote_count,
                voteAverage = it.vote_average,
                favourite = false
            ))
        }
        return movies
    }

    override suspend fun saveAsFavourite(movie: Movie): Long {
        return moviesDao.insertFavourite(
            MovieEntity(
                movie.id,
                movie.image,
                movie.title,
                movie.releaseDate,
                movie.overview,
                true
            )
        )
    }

    override suspend fun deleteFavourite(movieId: Int): Int {
        return moviesDao.deleteFavourite(movieId)
    }
}
