package com.jakubmateusiak.tmbdrecrutmenttask.data.api.services

import com.jakubmateusiak.tmbdrecrutmenttask.data.api.RESTClient
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.MovieSearchResponse
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.NowPlayingResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {


    @GET("movie/now_playing")
    fun getNowPlaying(@Query("api_key") apiKey: String = RESTClient.API_KEY):
            Deferred<Response<NowPlayingResponse>>

    @GET("search/movie")
    fun search(@Query("api_key") apiKey: String = RESTClient.API_KEY, @Query("query") query: String):
            Deferred<Response<MovieSearchResponse>>
}
