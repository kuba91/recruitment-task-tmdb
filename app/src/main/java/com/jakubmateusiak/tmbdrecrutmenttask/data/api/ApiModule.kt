package com.jakubmateusiak.tmbdrecrutmenttask.data.api

import com.jakubmateusiak.tmbdrecrutmenttask.data.api.RESTClient.createOkHttpClient
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.RESTClient.createRetrofit
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.RESTClient.createWebService
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.datasource.RemoteDataSource
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.services.MovieService
import org.koin.dsl.module

val apiModule = module {
    single { createOkHttpClient(get()) }
    single { createRetrofit(get(), "https://api.themoviedb.org/3/") }
    single { createWebService<MovieService>(get()) }
    single { RemoteDataSource(get()) }
}
