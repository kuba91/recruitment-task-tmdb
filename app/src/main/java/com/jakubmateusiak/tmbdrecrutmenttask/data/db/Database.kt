package com.jakubmateusiak.tmbdrecrutmenttask.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.dao.MoviesDao
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.entities.MovieEntity

@Database(
    entities = [MovieEntity::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun moviesDao(): MoviesDao
}