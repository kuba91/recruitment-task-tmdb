package com.jakubmateusiak.tmbdrecrutmenttask.data.api.datasource

import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.ApiResult
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.MovieSearchResponse
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.models.NowPlayingResponse
import com.jakubmateusiak.tmbdrecrutmenttask.data.api.services.MovieService
import retrofit2.Response

class RemoteDataSource(
    private val movieService: MovieService
) {

    suspend fun getNowPlaying(): ApiResult<NowPlayingResponse> {
        val response = movieService.getNowPlaying().await()
        return parseResponse(response)
    }

    suspend fun search(query: String): ApiResult<MovieSearchResponse> {
        val response = movieService.search(query = query).await()
        return parseResponse(response)
    }

    private inline fun <reified T : Any> parseResponse(response: Response<T>): ApiResult<T> {
        if (response.isSuccessful) {
            response.body()?.let {
                return ApiResult.Success(it)
            }
        } else {
            try {
                response.errorBody()?.let {
                    return ApiResult.Error(it.string())
                }
            } catch (e: java.lang.Exception) {
                return buildGeneralApiError()
            }
        }
        return buildGeneralApiError()
    }

    // todo translate error
    private fun buildGeneralApiError(): ApiResult.Error {
        return ApiResult.Error(
            "Unknown error"
        )
    }
}
