package com.jakubmateusiak.tmbdrecrutmenttask.data.db

import androidx.room.Room
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.dao.MoviesDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    single<AppDatabase> {
        Room.databaseBuilder(
            androidApplication(), AppDatabase::class.java,
            "app_database"
        )
            .fallbackToDestructiveMigration()
            .build()
    }
    single<MoviesDao> { (get() as AppDatabase).moviesDao() }
}