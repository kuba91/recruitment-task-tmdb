package com.jakubmateusiak.tmbdrecrutmenttask.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jakubmateusiak.tmbdrecrutmenttask.data.db.entities.MovieEntity

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavourite(movie: MovieEntity) : Long

    @Query("delete from ${MovieEntity.TABLE_NAME} where id = :id")
    fun deleteFavourite(id: Int) : Int

    @Query("select id from ${MovieEntity.TABLE_NAME}")
    fun selectFavourites(): LiveData<List<Int>>
}