package com.jakubmateusiak.tmbdrecrutmenttask.data.api.models

import androidx.annotation.Keep

@Keep
data class Dates(
    val maximum: String,
    val minimum: String
)
