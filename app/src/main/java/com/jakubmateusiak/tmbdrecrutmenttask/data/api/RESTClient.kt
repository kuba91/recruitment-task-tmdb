package com.jakubmateusiak.tmbdrecrutmenttask.data.api

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RESTClient {

    const val IMAGE_PATH = "https://image.tmdb.org/t/p/w500/"
    const val API_KEY = "2b09aba6715c8a23be30028ebb045f1a" //todo store in separate file

    fun createOkHttpClient(context: Context): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val builder = createOkHttpClientBuilder(context)
            .addInterceptor(httpLoggingInterceptor)
        return builder.build()
    }

    private fun createOkHttpClientBuilder(
        context: Context
    ): OkHttpClient.Builder {
        val timeout = 30L
        val client = OkHttpClient()
        return client.newBuilder()
            .callTimeout(timeout, TimeUnit.SECONDS)
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
    }

    fun createRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    inline fun <reified T> createWebService(retrofit: Retrofit): T {
        return retrofit.create(T::class.java)
    }
}
